(
    cd "$(dirname "${0}")"
    TERRAFORM_VERSION=0.13.4

    if [ ! -e "terraform" ]; then
        wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -O terraform.zip
        unzip terraform.zip
        rm terraform.zip
    fi
)

export PATH=${PATH}:$(dirname $0)
